package vikash.assignment.main;

import vikash.assignment.data.Test;
import vikash.assignment.singleton.Test2;

public class main {

	public static void main(String[] args) {
		
		Test testObj = new Test();
		
		testObj.tostring(); //default initialization
							
		// error, java do not perform default initialization for local variable.
		// testObj.newm(); 

		 Test2 nTest2 = Test2.inputString("New string");
		   nTest2.print();
	}

}