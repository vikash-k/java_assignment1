package vikash.assignment.data;

public class Test {
	int a;
	char ch;
	/**
	 * to print the class variable
	 */
	public void tostring() {
		System.out.println(" Integer: " + a + " Character: " + ch);
	}
    //method for local variable
	public void newm() {
		int p;
		int l;
		// error, java do not perform default initialization for local variable.
		// System.out.println( p + " " + l);
	}
}
