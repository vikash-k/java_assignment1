package vikash.regexjtest;

import static org.junit.Assert.*;

import org.junit.Test;

import vikash.regexmatching.*;


public class UnitTestCaseRE {
	@Test
	public void testp() {
		//RegularExpression junit=new RegularExpression();

		assertTrue(RegularExpression.checkRegexPattern("Hello world."));
		assertTrue(RegularExpression.checkRegexPattern("Hello_world."));
		assertFalse(RegularExpression.checkRegexPattern("Hello World"));
		assertFalse(RegularExpression.checkRegexPattern("Hello. World"));
		assertFalse(RegularExpression.checkRegexPattern("hellow_wold."));
		//nullvalue
		assertFalse(RegularExpression.checkRegexPattern(" ")); 
	}



}
