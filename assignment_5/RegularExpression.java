/**
 * Java Assignment day 5.  
 * To check the given string is begin with capital letter and end with a period
 */

import java.util.Scanner;
import java.util.regex.*;

public class RegularExpression {
	/**
	 * 
	 * @param p
	 * @return true If p matches with the regex pattern, else false
	 */
	public static boolean checkRegexPattern(String p) {

		// regex pattern
		Pattern p1 = Pattern.compile("[A-Z][^A-Z]*[.]$");
		Matcher match = p1.matcher(p);

		return match.find();
	}

	// main method
	public static void main(String[] args) {
		String str;

		Scanner scan = new Scanner(System.in);
		System.out.println("String: ");
		str = scan.nextLine();

		while (!(str.equalsIgnoreCase("exit"))) { // exit to terminate

			if (checkRegexPattern(str)) {
				System.out.println("Matched");
			} else {
				System.out.println("Not matched");
			}
			str = scan.nextLine();
		}
		scan.close();
	}
}
