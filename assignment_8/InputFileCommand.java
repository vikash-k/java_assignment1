/*Java Assignment day 8.
 * This code take file input from the command line arguement and print the number of different charactors found in 
 * the file into output.txt file.
 * 
 *  
 * 
 * 
 */

import java.util.*;
import java.io.*;

class InputFileCommand{
	

    public static void main(String[] args) throws FileNotFoundException
    {
    	
    		Map<Character, Integer> characters = new TreeMap<Character, Integer>();
    	
    		File inFile = null;
    		if (0 < args.length) {
    			inFile = new File(args[0]);
    			Scanner input= new Scanner(inFile);
   
           
    		while (input.hasNext()) // while there is more to read
    		{
       	
    			char[] line = input.nextLine().toLowerCase().toCharArray(); // reading characters line by line
       	
    					for (Character character : line) {
       		 
    							if (Character.isLetter(character)){
               	 
    								if (characters.containsKey(character)) {
                   	 
    										characters.put(character, characters.get(character) + 1);
    								} 
    								else {
    									
    									characters.put(character, 1);
    								}
                    
    							}
    					}
    		}
       			
    		if (input != null){  
       	
    			input.close();// closing the scanner
    		}
       			
    		File f = new File("output.txt"); //to take output of consol to the output.txt file
       
    		FileOutputStream fos = new FileOutputStream(f); //Creates a file output stream to write
       
    		PrintStream ps = new PrintStream(fos);
    		System.setOut(ps);
       		
    		if (!characters.isEmpty()){
       	
    			for (Map.Entry<Character, Integer> entry : characters.entrySet()) {
       		
    				System.out.println(entry.getKey() + ": " + entry.getValue());
    			}
    		}

    		} 
    		
    	else {// if argument not found then print invalid argument 
    		System.err.println("Invalid arguments count:" + args.length);
    		
    		System.exit(0);
    	}
    }
}
