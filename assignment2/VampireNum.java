public class VampireNum {
    private int v = 0;
    private int x = 0;
    private int y = 0;
     
    public int getVampire() {      // to get the value of vampire Number
        return v;
    }

    public int getX() {           // Vampire number =X*Y , so here we are getting the value of X.
        return x;
    }
 
    public int getY() {           //Vampire number =X*Y , so here we are getting the value of Y.
        return y;
    }

    public boolean isVampire(int number){     // check for vampire number
    	
    	int digit=(int) (Math.log(number) / Math.log(10)); // get number of digits
    	
    	
        int[] part = new int[digit+1];
        int p=0, Nmb;
        Nmb=number;
       
        while(Nmb !=0){
        	part[p]= Nmb%10;
        	Nmb=Nmb/10;
        	
        	p=p+1; 	
         }

    	int result= 0;
    	int x1=0;
    	int y1=0;
    	
    	if(p==4){ // for four digit number
    	    for (int i = 0; i < part.length; i++)
    	    {
    	        for (int j = 0; j < part.length; j++)
    	        {
    	            if (j == i) continue;
    	            for (int k = 0; k < part.length; k++)
    	            {
    	                if ((k == i) || (k == j)) continue;
    	                
    	                for (int l = 0; l < part.length; l++)
    	                {
    	                	if ((l == i) || (l == j) || (l == k)) continue;
    	                		x1 = part[i] * 10+ part[j];
    	                		y1 = part[k] * 10 + part[l];
                            result =x1*y1;                    	
                             if (result == number)
                             {
                            	 this.x=x1;
                            	 this.y=y1;
                            	 this.v=number;
                                return true;
                             }
    	                }
    	            }
    	        }
    	    }
    }
    	else{
    		//for 6 digit number
    for (int i = 0; i < part.length; i++)
    {
        for (int j = 0; j < part.length; j++)
        {
            if (j == i) continue;
            
            for (int k = 0; k < part.length; k++)
            {
                if ((k == i) || (k == j)) continue;
                
                for (int l = 0; l < part.length; l++)
                {
                    if ((l == i) || (l == j) || (l == k)) continue;
                    
                    for (int f= 0; f< part.length; f++){
                    	
                    	if ((f == i) || (f== j) || (f == k) || (f==l )) continue;
                    	
                    	for(int w =0; w< part.length; w++ ){
                    		
                    		if ((w == i) || (w== j) || (w == k) || (w==l )|| (w==f)||(part[k] ==part[w] )&&(part[w]==0)) continue;
                    		
                    			x1 = part[i] * 100+ part[j]*10+ part[k];
                    			y1 = part[l] * 100 + part[f]*10 +part[w];
                           
                    				result =x1*y1;                    	
                             if (result == number) // checking number for Vampire NUmber
                             {
                            	 this.x=x1;
                            	 this.y=y1;
                            	 this.v=number;
                                return true;
                             }
                    	}
                }
            }
        }
    } 
    }
    	}
        return false;
    }
    
    // main 
    
    public static void main(String[] args){
        VampireNum vnum = new VampireNum();
        int m=0;
        System.out.println("First 100 Vampire Number:");
       for(int number=1000; number<1000000; number++){
        	if(((int) (Math.log(number) / Math.log(10))+1)%2==0){   // check for even number of digit in the number
            if(vnum.isVampire(number)){
            	m=m+1;
                System.out.println(m +") " + vnum.getVampire() + " = " + vnum.getX() + " * " + vnum.getY());
               
               if(m==100)
                	break;
            }
        	}
        	}
        }
    }