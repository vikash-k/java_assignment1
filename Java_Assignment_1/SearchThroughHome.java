/**
 * Java Assignment 1. 
 * 
 * To make program to get a file in home directory which is matching a regular expression.
 * 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchThroughHome {

	static File SearchFile(File file, String s) {

		Pattern file1 = Pattern.compile(s);

		if (file.isDirectory()) {

			File[] arr = file.listFiles();
			for (File f : arr) {
				File found = SearchFile(f, s);

				if (found != null)
					return found;
			}
		} else {
			Matcher x = file1.matcher(file.getName());

			if (x.find()) {
				return file;
			}
		}

		return null;
	}

	public static void main(String[] args) throws FileNotFoundException {

		File NewFile = new File(System.getProperty("user.home"));

		File[] Listfile = NewFile.listFiles();

		Scanner n = new Scanner(System.in);

		char re = 'Y';
		String s;

		while ((re != 'n')) {

			System.out.println("Enter regular expression to search files");
			s = n.next();
			try {
				for (File p : Listfile) {

					File t = SearchFile(p, s);

					if (t != null) {

						System.out.println(t.getAbsolutePath());
					}
				}
			} catch (NullPointerException e) {
				System.out.println(e);
			}

			System.out.println("To continue press any key, To exit press N ");
			re = n.next().charAt(0);
			re = Character.toLowerCase(re);
		}
		n.close();
		System.out.println("Exit! ");

	}
}
