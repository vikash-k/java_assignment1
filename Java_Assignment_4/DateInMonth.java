
class DateInMonth{

/**
 * @param month and year
 * @return days
 */
	
	int days;
	DateInMonth(int month, int year){
		
		switch (month) {
        case 1:
            this.days = 31;
            break;
             
        case 2:
          
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) { //to check leap year
                this.days = 29;
            } else {
                this.days = 28;
            }
            break;
        case 3:
         
        		this.days = 31;
        		break;
        case 4:
            
        		this.days = 30;
        		break;
        case 5:
            
            	this.days = 31;
            	break;
        case 6:
        
        		this.days = 30;
        		break;
        case 7:
           
        		this.days= 31;
        		break;
        case 8:
           
        		this.days = 31;
        		break;
        case 9:
           
            	this.days = 30;
            	break;
        case 10:
           
            	this.days = 31;
            	break;
        case 11:
           
            	this.days = 30;
            	break;
        case 12:
      
            	this.days = 31;
		}
	}
	public DateInMonth(){ //constructor
		
	}
}