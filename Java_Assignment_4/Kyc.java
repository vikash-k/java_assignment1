/*
 * Java Assignment 4
 */
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Scanner;

public class Kyc {

	static boolean Rangefound(Calendar a, Calendar b){
		 if(((a.get(Calendar.YEAR)) > b.get(Calendar.YEAR)) || ((a.get(Calendar.YEAR)==b.get(Calendar.YEAR))&& ((a.get(Calendar.MONTH) >b.get(Calendar.MONTH))))||((a.get(Calendar.YEAR)==b.get(Calendar.YEAR)) && (a.get(Calendar.MONTH) == b.get(Calendar.MONTH))&& (a.get(Calendar.DATE) >= b.get(Calendar.DATE)))){
			
			 return false;
		 }
		 return true;
	}
	
	
	
	static void formDate(String p){ // date range for KYC
		
		
		DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
		dateFormatter.setLenient(false);
		DateInMonth x =new DateInMonth();
		try{
			String[] q =p.split(" ");
			Date date1 = dateFormatter.parse(q[0]);
			Date date3=dateFormatter.parse(q[0]);
			Date date2 = dateFormatter.parse(q[1]);
			Calendar calendar = Calendar.getInstance();
			Calendar calendar2 = Calendar.getInstance();
			Calendar calendar3 = Calendar.getInstance();
			calendar.setTime(date1);
			calendar2.setTime(date2);
			calendar3.setTime(date3);
	
			int l=0;
			int date = calendar.get(Calendar.DATE);
			int month =calendar.get(Calendar.MONTH) +1;
			int Year =calendar.get(Calendar.YEAR);
		
		// for before 30 days from aniversary
		    if(Rangefound(calendar, calendar2) &&(calendar2.get(Calendar.YEAR)-calendar.get(Calendar.YEAR)) >=1){
		    	calendar.add(Calendar.YEAR, (calendar2.get(Calendar.YEAR)-calendar.get(Calendar.YEAR)));
		    	 calendar3.add(Calendar.YEAR, (calendar2.get(Calendar.YEAR)-calendar3.get(Calendar.YEAR)));
		    	
					if(date==31){
						calendar.add(Calendar.DATE, -30);
					}else if(month==1 && date<30){
						calendar.add(Calendar.YEAR, -1);
						calendar.add(Calendar.MONTH, 11);
						calendar.add(Calendar.DATE, 1);
					}else{							
							calendar.add(Calendar.MONTH, -1);
							
							x=new DateInMonth(calendar.get(Calendar.MONTH) +1, Year);
							
							if(x.days==31){
								calendar.add(Calendar.DATE, 1 );
							}else if(calendar2.get(Calendar.MONTH)==1){
								
								 if((calendar.get(Calendar.DATE) + x.days)<=30){
										calendar.add(Calendar.MONTH, -1);
										l=(30-x.days-calendar.get(Calendar.DATE));
										calendar.add(Calendar.DATE, (31-l) );
								 }else{
									    calendar.add(Calendar.DATE, (x.days-30));
								 }
							}						
					}	
					//after 30 days from aniversary
					x=new DateInMonth(month, Year);
					
					if(date==1 || x.days==31){
						calendar3.add(Calendar.DATE, 30);
					}else if(month==12 && date>30){
						calendar3.add(Calendar.YEAR, 1);
						calendar3.add(Calendar.MONTH, -11);
						calendar3.add(Calendar.DATE, -1);
					}else{		
											
							calendar3.add(Calendar.MONTH, 1);
							
							x=new DateInMonth(calendar3.get(Calendar.MONTH), calendar3.get(Calendar.YEAR));
							
							if(x.days==31 && (calendar3.get(Calendar.MONTH)!=1) ){
								calendar3.add(Calendar.DATE, -1 );
							}else if(calendar3.get(Calendar.MONTH)==1){
								DateInMonth x2 =new DateInMonth(2,Year);
								 if((31-calendar3.get(Calendar.DATE) + x2.days)<30){
										calendar3.add(Calendar.MONTH, 1);
										l=(31+1-calendar3.get(Calendar.DATE));
										calendar3.add(Calendar.DATE, -(31-l) );
								 }else{
									    calendar3.add(Calendar.DATE, -1);
								 }
							}
							else{
								
								l=30-x.days;
								calendar3.add(Calendar.DATE, l );
							}
	
					}
					if(Rangefound(calendar, calendar2)){
		                if(Rangefound(calendar3, calendar2)){ //checking dates
		                	System.out.println( dateFormatter.format(calendar.getTime()) + " " + dateFormatter.format( calendar3.getTime()));
		                }
		                else{
		                	System.out.println( dateFormatter.format(calendar.getTime()) + " " + dateFormatter.format( calendar2.getTime()));
		                    
		                }
					}else if(calendar.get(Calendar.YEAR)!=Year){
						calendar.add(Calendar.YEAR, -1);
						calendar3.add(Calendar.YEAR, -1);
						System.out.println( dateFormatter.format(calendar.getTime()) + " " + dateFormatter.format( calendar3.getTime()));
	                    
					}else{
						System.out.println("No range");
					}
												
		    }else{
				System.out.println("No range");
			}
		}catch(Exception e){
			System.out.println(e);
		}
	}
	
	//main method
	public static void main(String[] args) {
		Scanner sc1 =new Scanner(System.in);
		Scanner sc =new Scanner(System.in);
		
		System.out.println("Test Case No:");
		int l =sc1.nextInt();
	
		System.out.println(l);
		String s=" ";
		s=sc.nextLine();
		System.out.println(" ");
		while(l>0){
			
			formDate(s);
			s=sc.nextLine();
			l=l-1;
		}
		
		sc1.close();
		sc.close();
		}
	
	
}
