/*
 * java assignment 3 question 4
 * 
 * Cycle_1, FactoriesOfCycle are two independent interfaces
 * there is three classes for the factories of cycle.(UnicycleFactory,BicycleFactory and TricycleFactory  )
 * Also three classes for Cycle(Unicycle_1, Bicycle_1, Tricycle_1) which impliments Cycle_1 interface
 */

interface Cycle_1 {
	void balance();
}

interface FactoriesOfCycle {
	Cycle_1 getCycle();
}

class Unicycle_1 implements Cycle_1 {
	public void balance() { System.out.println("Single wheel: difficult to balance"); }
}

class UnicycleFactory implements FactoriesOfCycle {
	public Cycle_1 getCycle() {
		return new Unicycle_1();
	}
}

class Bicycle_1 implements Cycle_1 {
	public void balance() { System.out.println("Two wheels: Moderate to balance"); }
}

class BicycleFactory implements FactoriesOfCycle {
	public Cycle_1 getCycle() {
		return new Bicycle_1();
	}
}


class Tricycle_1 implements Cycle_1 {
	public void balance(){ System.out.println("Three wheels: easy to balance"); }
	
}

class TricycleFactory implements FactoriesOfCycle {
	public Cycle_1 getCycle() {
		return new Tricycle_1();
	}
}

public class Cycles {
	public static void rideCycle(FactoriesOfCycle factory) {
		Cycle_1 c = factory.getCycle();
		c.balance();
	}
	public static void main(String [] args) {
		rideCycle(new UnicycleFactory());
		rideCycle(new BicycleFactory());
		rideCycle(new TricycleFactory());	
	}
}