/*
 * java assignment 3 question 5
 * 
 * inner class is class inside outerClass class
 * 
 * second class name is innerclass in which inner2 is the inner class
 * which is inherited from inner class of outerClass class
 */
class outerClass{
	
	class inner{
		void innert(String s){
			System.out.println(s);
		}
	}
}


public class innerclass {
	 public class inner2 extends outerClass.inner{
		  inner2(outerClass l){
			 l.super();
			 System.out.println("inner class inheritance");
		 }
		 
	 }
		
	public static void main(String[] args){
		
		outerClass p= new outerClass();
		innerclass q= new innerclass();
		q.new inner2(p);
		
	}
}

