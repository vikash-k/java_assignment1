//
/*
 * Java Assignment 3, question 1
 * Rodent is class from which Mouse, Gerbil and Hamester class are inherited with overriding method
 * 
 * 
 */
class Rodent{
	void descriptionR(){};
      
}    

class Mouse extends Rodent{
       
     void descriptionR(){
    		System.out.println("Mouse");
	}
}

class Gerbil extends Rodent{
	void descriptionR(){
		System.out.println("Gerbil");
	}
}
 class Hamester extends Rodent{
	 void descriptionR(){
		 System.out.println("Hamsters");
		
	}
}

  class main{
	public static void main(String[] args){
		Rodent[] x ={new Rodent(), new Mouse(), new Gerbil(), new Hamester()};
		for(Rodent i : x){	//Calling base-class methods
			i.descriptionR();
		}
			
		}
	}