/*
 * Java Assignment 3, question 3
 * A,B, C are interfaces with 2 methods each.
 * D interface inherit A,B and C interface
 * NewClass is class and Nclass is inherited class from NewClass, which implements D interface
 * MultipleInterface is main class in which there is four method take all the
 * different interfaces (A,B,C & D)
 * p is Nclass type, which is called in each method for interface input.
 */


interface A{
	 void Abck();
	void Afont();
}
interface B{
	void Bbck();
	void Bfont();
}
interface C{
	void Cbck();
	void Cfont();
}
interface D extends A,B,C{
	void Dnew();
}
class NewClass{
	void NewMethod(){
		System.out.println("Concrete class");
	}
}

class Nclass extends NewClass implements D{

	public void Abck() {
		// TODO Auto-generated method stub
		System.out.println("Abck();");
		
	}

public void Bbck() {
		// TODO Auto-generated method stub
	System.out.println("Bbck();");
	}

	

	public void Cbck() {
		// TODO Auto-generated method stub
		System.out.println("Cbck();");
	}

	public void Dnew() {
		// TODO Auto-generated method stub
		System.out.println("Dnew();");
		
	}



	public void Afont() {
		// TODO Auto-generated method stub
		System.out.println("Afont();");
		
	}



	public void Bfont() {
		// TODO Auto-generated method stub
		
		System.out.println("Bfont();");
		
	}



	public void Cfont() {
		// TODO Auto-generated method stub
		System.out.println("Cfont();");
		
	}
	
}

public class MultipleInterfaces {
	 public static void I1(A a){
		 a.Abck();
	}
	 public static void I2(B b){
		 b.Bbck();
	}
	 public static void I3(C c){
		 c.Cbck();
	}
	 public static void I4(D d){
		 d.Abck();
	}
	public static void main(String[] args){
		
		Nclass p =new Nclass();
		   I1(p);
		   I2(p);
		   I3(p);
		   I4(p);
		
	}
}
