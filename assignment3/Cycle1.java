/*
 * Java Assignment 3, question 2
 * 
 * Unicycle , Bicycle and Tricycle are class which inherited from Cycle class
 * balance method is only in Bicycle and Unicycle
 * Using the upcasting and downcasting method balance method has been called
 */
class Cycle{
	void type(){
		System.out.println("type of cycle");	
	}
	

}

class Unicycle extends Cycle{  // subclass
	void balance(){
		System.out.println("Balance method for Unicycle!!");
	}


}

class Bicycle extends Cycle{    // subclass
	void balance(){
		System.out.println("Balance method for Bicycle!!");
	}


}

class Tricycle extends Cycle{     // subclass

	void nobalance(){
		System.out.println("No balance method require!!");
	}

}

class Cycle1{

public static void main(String[] args){
	
	// upcasting
	Cycle[] c1 = {new Unicycle(), new Bicycle(), new Tricycle()};  
	
	//for(Cycle i:c1)
		//i.balance();	/* error, there is no balance method in Cycle.


	/*downcasting
		Error:- only for Tricycle because it has no any balance method. 
			*/
	Unicycle u1=(Unicycle)c1[0];
	Bicycle b1=(Bicycle)c1[1];
	Tricycle t1 =(Tricycle)c1[2];  

	u1.balance();
	b1.balance();
	//t1.balance();
	
}
}