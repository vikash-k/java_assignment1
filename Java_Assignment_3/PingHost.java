/**
 * 
 * Java Assignment 3.
 * To ping a host and calculate the median of the time to ping
 * 
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.*;


class PingHost {
	 
	   public static void runHost(String command, int x) {

		   try {
		
			   		Process run = Runtime.getRuntime().exec(command);
			   		InputStreamReader inputStrem =new InputStreamReader(run.getInputStream());
			   		BufferedReader inp = new BufferedReader(inputStrem);

			   		String s = "";
			   		double[] d= new double[x];
			   		int i=0,flag=0;
			   		Pattern p = Pattern.compile("(/*?)time=(.*?)ms");
			
			   		while ((s = inp.readLine()) != null) {
			   			Matcher m= p.matcher(s);
			   			//System.out.println(s);  
			   			while( m.find() )
			   			{
			   					
			   					d[i]=Double.parseDouble( m.group(2));
			   					i++;
			   					flag=1;
			   			}
			   		}
			   		if(flag==1){
			   			Arrays.sort(d); // to sort the array of time
			   			if(x%2 ==0){				
			   				System.out.println("Median of ping time:" + (d[x/2] + d[(x/2) -1])/2 + " ms");
			   			}else
			   				System.out.println("Median of ping time: " + d[(x-1)/2] + "ms");
			   		}else{
			   			System.out.println(" Sorry!, it's not valid ");
			   		}
			
		   } catch (Exception e) {		
			   		System.out.println("enter a valid host");
		      }
	 }

	   public static void main(String[] args) {
		   		Scanner sc =new Scanner(System.in);
		   		System.out.println("Enter an host");
		   		String ip = sc.nextLine();
		   		runHost("ping " + ip + " -c 10" ,10);  // 10 is denoting packet of data
		   		sc.close();
	   }
}