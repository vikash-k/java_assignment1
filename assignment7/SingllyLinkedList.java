/**
 * Java Assignment 6-7. Singly linkedlist to implement insertion and deletion 
 * 
 */



import java.util.Scanner;


class SList<S> {   
	 Linklist<S> head = new Linklist<S>(null);
	
	SListIterator<S> iterator() { 
		
		return new SListIterator<S>( head); 
	}
	
	public String toString() {   // print method for linkedlist
		
		if(head.next == null) {
			return "Linkedlist : ";
		}
		System.out.print("Linkedlist : ");
		
		SListIterator<S> it = this.iterator();
		
		StringBuilder s = new StringBuilder(); 
		
		while(it.hasNext()) {
			s.append(it.next() + (it.hasNext() ? ", " : ""));
		}
		return "[" + s + "]";
	}		
}

class SListIterator<S> {
	Linklist<S> current;
	SListIterator(Linklist<S> link) {
		current = link;
	}
	public boolean hasNext() {  // method to check, linkedlist is empty or not
		return current.next != null;
	}
	public Linklist<S> next() {
		current = current.next;
		return current;
	}
	public void insert(S newStr) {  //Inserting newStr at head position
		 
		
		current.next = new Linklist<S>(newStr, current.next);
		current = current.next;
	}	
	public void remove() {                     // remove head of list
		if(current.next != null) {
			current.next = current.next.next;
		}
	}
}

class Linklist<S> {
	S nList;
	Linklist<S> next;
	Linklist(S e, Linklist<S> next) { 
		this.nList = e;
		this.next = next; 
	}
	Linklist(S e) {
		this(e, null);
	}	
	public String toString() {
		if(nList == null) return "null";
		return nList.toString();
	}
}

public class SingllyLinkedList {
	public static void main(String[] args) {
		SList<String> slst = new SList<String>();
		SListIterator<String> slIst = slst.iterator();
		SListIterator<String> slIst2 = slst.iterator();
		Scanner sc =new Scanner(System.in);
		String s="";
		
		System.out.println("String to insert!");
		s=sc.next();
		slIst.insert(s);
		System.out.println( slst );
		char p='y';
		try{
		while(Character.toLowerCase(p)!='n'){ // to exit the program
			
			System.out.println("insert(press 1) or delete (press other integer)?");	
			int l =sc.nextInt();
			if(l!=1){
				slIst2.remove();
				System.out.println( slst);
				
				
			}else{
				System.out.println("Enter string!");
				s=sc.next();
				slIst.insert(s);
				System.out.println( slst );
				
			}
			System.out.println(" exit(Press N), continue(any other key)");
			p=sc.next().charAt(0);
		}
		sc.close();
		}catch (Exception e){
			System.out.println(e);
		}
			
	}

}