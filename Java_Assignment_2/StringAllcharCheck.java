

/*
 * Java Assignment 2.  Write a program to check all the alphabates in a string. space complexity O(27) and time complexity O(n)
 * 
 */

import java.util.*;


public class StringAllcharCheck {
	
	public static void main(String[] args){
		
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the string:-");
		String s=sc.nextLine();
		
		
		int flag=1;
		
		int[] p=new int[26];
			
			for(int i=0; i<s.length(); i++){
				
				if(Character.isLetter(s.charAt(i))){
					p[Character.toLowerCase((s.charAt(i)))-'a'] =1;
				}
			}
			
			for(int i=0; i<13; i++){
					
					if(p[i]*p[25-i]==0){
						flag =0;
						break;
					}
			}
			
			if(flag==1){
				System.out.println("All the alphabates are FOUND!!");
			}
			else{
				System.out.println("All the alphabates are NOT FOUND!!");
			}
			sc.close();
		}
		
}
